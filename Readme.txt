
    Drvier and example for lpms-be1 motion sensor
    Specially for SPI and I2C communication
    Building on Mbed Studio with library mbed-os 5.15.0

Files:
    /src/lpms_be1.* : Drvier for lpms-be1 motion sensor
    /src/main.c : Main boby example for reading be1 data

    
    
   
    
    Copyright (C) 2020 LP-Research
    All rights reserved.
    Contact: LP-Research (info@lp-research.com)