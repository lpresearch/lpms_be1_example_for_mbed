#include "mbed.h"
#include "lpms_be1.h"


Serial serial_port(SERIAL_TX, SERIAL_RX);
LPMS_BE1 lpms_be1;
sensor_data_t sensor_data;


void be1_config_test(void);
void be1_read_data(void);
void be1_print_data(void);

void delay_ms(uint32_t ms);



int main()
{
    serial_port.baud(115200);
    lpms_be1.setup();

    while(1) {
        be1_read_data();
        be1_print_data();
        delay_ms(1000);
    }
}


void be1_config_test(void)
{ 
    uint8_t buf[9] = { 0 };
    lpms_be1.get_reg_data(0x00, buf, 9);
    for (int i = 0; i < 9; i++) {
        serial_port.printf("%02x ", buf[i]);
    }
    serial_port.printf("\r\n");

    lpms_be1.setup();

    lpms_be1.get_reg_data(0x00, buf, 9);
    for (int i = 0; i < 9; i++) {
        serial_port.printf("%02x ", buf[i]);
    }
    serial_port.printf("\r\n");
    serial_port.printf("\r\n");
}

void be1_read_data(void)
{
    lpms_be1.get_timestamp(&sensor_data.timestamp);
    lpms_be1.get_acc(sensor_data.acc);
    lpms_be1.get_gyro(sensor_data.gyro);
    lpms_be1.get_quat(sensor_data.quat);
    lpms_be1.get_euler(sensor_data.euler);
}
void be1_print_data(void)
{
    float ts = sensor_data.timestamp * 0.002;
    serial_port.printf("timestamp: %.3f\r\n", ts);
    serial_port.printf("acc: %.3f, %.3f, %.3f\r\n", sensor_data.acc[0], sensor_data.acc[1], sensor_data.acc[2]);
    serial_port.printf("gyro: %.3f, %.3f, %.3f\r\n", sensor_data.gyro[0], sensor_data.gyro[1], sensor_data.gyro[2]);
    serial_port.printf("quat: %.3f, %.3f, %.3f, %.3f\r\n", sensor_data.quat[0], sensor_data.quat[1], sensor_data.quat[2], sensor_data.quat[3]);
    serial_port.printf("euler: %.3f, %.3f, %.3f\r\n", sensor_data.euler[0], sensor_data.euler[1], sensor_data.euler[2]);
    serial_port.printf("\r\n");
}

void delay_ms(uint32_t ms)
{
    while (ms--) {
        wait_us(1000);
    }
}