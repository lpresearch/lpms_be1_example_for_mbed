/***********************************************************************
**
** Copyright (C) 2020 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
***********************************************************************/

#include "lpms_be1.h"



LPMS_BE1::LPMS_BE1()
{
#if defined BE1_USE_SPI

    be1_spi = new SPI(SPI_MOSI, SPI_MISO, SPI_SCK);
    be1_spi->format(8, 0);
    be1_spi->frequency(BE1_SPI_FREQUENCY);
    spi_cs = new DigitalOut(SPI_CS);

#elif defined BE1_USE_I2C

    be1_i2c = new I2C(I2C_SDA, I2C_SCL);
    be1_i2c->frequency(BE1_I2C_FREQUENCY);

#endif
}


void LPMS_BE1::setup(void)
{
    set_config_enable();
    wait_update();

    set_data_output(LPMS_ALL_DATA_EN);
    wait_update();

    set_data_freq(LPMS_DATA_FREQ_100HZ);
    wait_update();

    // Some other configurations here

    set_config_disable();
    wait_update();
}





void LPMS_BE1::set_config_enable(void)
{
    uint8_t data = 0x80;
    write_register(FUN_CONFIG, &data, 1);
}

void LPMS_BE1::set_config_disable(void)
{
    uint8_t data = 0x00;
    write_register(FUN_CONFIG, &data, 1);
}

void LPMS_BE1::set_data_output(uint8_t data_en)
{
    uint8_t data = data_en & LPMS_DATA_EN_MASK;
    write_register(DATA_ENABLE, &data, 1);
}

be1_status_t LPMS_BE1::set_data_freq(uint8_t freq)
{
    if (freq != LPMS_DATA_FREQ_5HZ &&
        freq != LPMS_DATA_FREQ_10HZ &&
        freq != LPMS_DATA_FREQ_50HZ &&
        freq != LPMS_DATA_FREQ_100HZ &&
        freq != LPMS_DATA_FREQ_250HZ &&
        freq != LPMS_DATA_FREQ_500HZ)
        return BE1_ERROR;

    uint8_t data = 0;
    read_register(DATA_CTRL, &data, 1);
    data &= ~LPMS_DATA_FREQ_MASK;
    data |= freq;
    write_register(DATA_CTRL, &data, 1);
    return BE1_OK;
}

be1_status_t LPMS_BE1::set_acc_range(uint8_t range)
{
    if (range != LPMS_ACC_FS_2G &&
        range != LPMS_ACC_FS_4G &&
        range != LPMS_ACC_FS_8G &&
        range != LPMS_ACC_FS_16G)
        return BE1_ERROR;

    uint8_t data = 0;
    read_register(CTRL_0_A, &data, 1);
    data &= ~LPMS_ACC_FS_MASK;
    data |= range;
    write_register(CTRL_0_A, &data, 1);
    return BE1_OK;
}

be1_status_t LPMS_BE1::set_gyro_range(uint8_t range)
{
    if (range != LPMS_GYR_FS_125DPS &&
        range != LPMS_GYR_FS_250DPS &&
        range != LPMS_GYR_FS_500DPS &&
        range != LPMS_GYR_FS_1000DPS &&
        range != LPMS_GYR_FS_2000DPS)
        return BE1_ERROR;

    uint8_t data = 0;
    read_register(CTRL_1_G, &data, 1);
    data &= ~LPMS_GYR_FS_MASK;
    data |= range;
    write_register(CTRL_1_G, &data, 1);
    return BE1_OK;
}





void LPMS_BE1::get_reg_data(uint8_t addr, uint8_t *buf, uint8_t len)
{
    read_register(addr, buf, len);
}

void LPMS_BE1::get_timestamp(uint32_t *ts)
{
    uint8_t buf[4] = { 0 };
    read_register(TIMESTAMP_0, buf, 4);
    *ts = uint8_to_uint32(buf);
}

void LPMS_BE1::get_acc(float *acc)
{
    uint8_t buf[12] = { 0 };
    read_register(ACC_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        acc[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_BE1::get_gyro(float *gyro)
{
    uint8_t buf[12] = { 0 };
    read_register(GYR_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        gyro[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_BE1::get_quat(float *quat)
{
    uint8_t buf[16] = { 0 };
    read_register(QUAT_W_0, buf, 16);
    for (int i = 0; i < 4; i++) {
        quat[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_BE1::get_euler(float *euler)
{
    uint8_t buf[12] = { 0 };
    read_register(EULER_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        euler[i] = uint8_to_float(&buf[i*4]);
    }
}








float LPMS_BE1::uint8_to_float(uint8_t *pu8vals)
{
    data_decoder decoder;
    for(int i = 0; i < 4; i++) {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.fval;
}

uint32_t LPMS_BE1::uint8_to_uint32(uint8_t *pu8vals)
{
    data_decoder decoder;
    for(int i = 0; i < 4; i++) {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.u32val;
}









void LPMS_BE1::read_register(uint8_t addr, uint8_t *buf, uint8_t len)
{
#if defined BE1_USE_SPI

    be1_spi->lock();
    spi_cs->write(0);
    wait_us(10);

    addr |= 0x80;
    be1_spi->write(addr);
    wait_us(10);
    for (int i = 0; i < len; i++) {
        buf[i] = be1_spi->write(0x00);
    }

    spi_cs->write(1);
    wait_us(10);
    be1_spi->unlock();

#elif defined BE1_USE_I2C

    be1_i2c->lock();
    be1_i2c->write((BE1_I2C_ADRRESS << 1), (const char *)&addr, 1, true);
    be1_i2c->read((BE1_I2C_ADRRESS << 1), (char *)buf, len);
    be1_i2c->unlock();

#endif
}

void LPMS_BE1::write_register(uint8_t addr, uint8_t *buf, uint8_t len)
{
#if defined BE1_USE_SPI

    be1_spi->lock();
    spi_cs->write(0);
    wait_us(10);

    addr &= ~0x80;
    be1_spi->write(addr);
    wait_us(10);
    for (int i = 0; i < len; i++) {
        be1_spi->write(buf[i]);
    }

    spi_cs->write(1);
    wait_us(10);
    be1_spi->unlock();

#elif defined BE1_USE_I2C

    be1_i2c->lock();
    be1_i2c->write((BE1_I2C_ADRRESS << 1), (const char *)&addr, 1, true);
    be1_i2c->write((BE1_I2C_ADRRESS << 1), (const char *)buf, len);
    be1_i2c->unlock();

#endif
}

void LPMS_BE1::wait_update(void)
{
    wait_us(2000);  // wait at least 2ms for updating registers
}
void LPMS_BE1::wait_reset(void)
{
    wait_us(10000); // wait at least 10ms for sensor reset
}
void LPMS_BE1::wait_reboot(void)
{
    wait_us(80000); // wait at least 80ms for sensor reboot
}
